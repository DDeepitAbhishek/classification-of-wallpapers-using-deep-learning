clc;
clear;

dataDir= './data/wallpapers/';

rng(1) % For reproducibility
Symmetry_Groups = {'P1', 'P2', 'PM' ,'PG', 'CM', 'PMM', 'PMG', 'PGG', 'CMM',...
    'P4', 'P4M', 'P4G', 'P3', 'P3M1', 'P31M', 'P6', 'P6M'};

train_folder = 'train';
test_folder  = 'test';
% uncomment after you create the augmentation dataset
%train_folder = 'train_aug';
%test_folder  = 'test_aug';
fprintf('Loading Train Filenames and Label Data...'); t = tic;
train_all = imageDatastore(fullfile(dataDir,train_folder),'IncludeSubfolders',true,'LabelSource',...
    'foldernames');
train_all.Labels = reordercats(train_all.Labels,Symmetry_Groups);
fprintf('Done in %.02f seconds\n', toc(t));

fprintf('Loading Test Filenames and Label Data...'); t = tic;
test = imageDatastore(fullfile(dataDir,test_folder),'IncludeSubfolders',true,'LabelSource',...
    'foldernames');
test.Labels = reordercats(test.Labels,Symmetry_Groups);
fprintf('Done in %.02f seconds\n', toc(t));
%% for train
mkdir('./data/wallpapers/train_original');
train_folder = 'train_original';
for i = 1:length(train_all.Files)
    train_image_128 = imresize(imread(train_all.Files{i}),[128,128]);

    train_name = strsplit(train_all.Files{i},'\');
    if ~exist(strcat('./data/wallpapers/train_original/',train_name{end-1}),'dir')
        mkdir(strcat('./data/wallpapers/train_original/',train_name{end-1}));
    end
    filename_train = strcat('./data/wallpapers/train_original/',train_name{end-1},'/',train_name{end});
    
    imwrite(train_image_128,filename_train);
    

end
%% For test

mkdir('./data/wallpapers/test_original');
test_folder = 'test_original';

for i = 1:length(train_all.Files)
    test_image_128 = imresize(imread(test.Files{i}),[128,128]);

    test_name = strsplit(test.Files{i},'\');
    if ~exist(strcat('./data/wallpapers/test_original/',test_name{end-1}),'dir')
        mkdir(strcat('./data/wallpapers/test_original/',test_name{end-1}));
    end
    
    filename_test = strcat('./data/wallpapers/test_original/',test_name{end-1},'/',test_name{end});
    
    imwrite(test_image_128,filename_test);

end
