%obtaining the augmented data
Symmetry_Groups = {'P1', 'P2', 'PM' ,'PG', 'CM', 'PMM', 'PMG', 'PGG', 'CMM',...
    'P4', 'P4M', 'P4G', 'P3', 'P3M1', 'P31M', 'P6', 'P6M'};
Symmetry_Groups = sort(Symmetry_Groups);
dataDir = './data/wallpapers/train/';
mkdir './data/wallpapers/train_aug'
new_location = './data/wallpapers/train_aug';
numIters = 5;
for k = 1 : numIters
    for i = 1 : length(Symmetry_Groups)
        train_path = strcat(dataDir,Symmetry_Groups{i},'/');
        image = fullfile(train_path,'*.png');
        image_inside = dir(image);
        for j = 1 : length(image_inside)
            filename = strcat(train_path,image_inside(j).name);
            I = imread(filename);
            [im_aug,Image_augment(k).(Symmetry_Groups{i}).rot(j),Image_augment(k).(Symmetry_Groups{i}).scale(j),Image_augment(k).(Symmetry_Groups{i}).tran{j}] = augmentImage(I);
            new_folder = strcat(new_location,'/',Symmetry_Groups{i},'/');
            if ~exist(new_folder, 'dir')
                mkdir(new_folder);
            end
            imwrite(im_aug,strcat(new_folder,num2str(k),image_inside(j).name));
        end
    end
end
%% Concatenating the data for all 17 classes
rotation = ([]);
scaling = ([]);
translation = ([]);

for j = 1:length(Symmetry_Groups)
    for k = 1:numIters
        rotation = cat(2,rotation,Image_augment(k).(Symmetry_Groups{j}).rot);
        scaling = cat(2,scaling,Image_augment(k).(Symmetry_Groups{j}).scale);
        translation = cat(2,translation,Image_augment(k).(Symmetry_Groups{j}).tran);
    end
end
%% Plotting Histograms
for i = 1:length(translation)
    X(i,1) = translation{i}(1);
    X(i,2) = translation{i}(2);
end
figure(1);
hist3(X);
title('Histogram of Translation');
xlabel('X-axis'); ylabel('Y-axis');
set(get(gca,'child'),'FaceColor','interp','CDataMode','auto');

figure(2);
hist(rotation);
title('Histogram of Rotation');
xlabel('Rotation Data');


figure(3);
hist(scaling);
title('Histogram of Scaling');
xlabel('Scaling Data');


    
   
    




                    
