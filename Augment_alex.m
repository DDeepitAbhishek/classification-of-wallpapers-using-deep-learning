clc;
clear;

dataDir= './data/wallpapers/';
checkpointDir = 'modelCheckpoints/alexnet';

rng(1) % For reproducibility
Symmetry_Groups = {'P1', 'P2', 'PM' ,'PG', 'CM', 'PMM', 'PMG', 'PGG', 'CMM',...
    'P4', 'P4M', 'P4G', 'P3', 'P3M1', 'P31M', 'P6', 'P6M'};

train_folder = 'train_aug';
test_folder  = 'test_aug';
fprintf('Loading Train Filenames and Label Data...'); t = tic;
train_all = imageDatastore(fullfile(dataDir,train_folder),'IncludeSubfolders',true,'LabelSource',...
    'foldernames');
train_all.Labels = reordercats(train_all.Labels,Symmetry_Groups);
fprintf('Done in %.02f seconds\n', toc(t));
fprintf('\nLoading Test Filenames and Label Data...'); t = tic;
test = imageDatastore(fullfile(dataDir,test_folder),'IncludeSubfolders',true,'LabelSource',...
    'foldernames');
test.Labels = reordercats(test.Labels,Symmetry_Groups);
fprintf('Done in %.02f seconds\n', toc(t));

%% Pre-Processing - Alex train aug
if ~exist('./data/wallpapers/train_aug_alex/','dir')
    mkdir('./data/wallpapers/train_aug_alex/');
end


for i = 1:length(train_all.Files)
    I = imread(train_all.Files{i});
    I1 = imresize(I,[227,227]);
    I2 = repmat(I1,[1,1,3]);
    train_name = strsplit(train_all.Files{i},'\');
    if ~exist(strcat('./data/wallpapers/train_aug_alex/',train_name{end-1}),'dir')
        mkdir(strcat('./data/wallpapers/train_aug_alex/',train_name{end-1}));
    end
    filename = strcat('./data/wallpapers/train_aug_alex/',train_name{end-1},'/',train_name{end});
    imwrite(I2,filename);
end
%% Preprocessing Alex Test Aug
if ~exist('./data/wallpapers/test_aug_alex/','dir')
    mkdir('./data/wallpapers/test_aug_alex/');
end

for i = 1:length(test.Files)
    I = imread(test.Files{i});
    I1 = imresize(I,[227,227]);
    I2 = repmat(I1,[1,1,3]);
    train_name = strsplit(test.Files{i},'\');
    if ~exist(strcat('./data/wallpapers/test_aug_alex/',train_name{end-1}),'dir')
        mkdir(strcat('./data/wallpapers/test_aug_alex/',train_name{end-1}));
    end
    filename = strcat('./data/wallpapers/test_aug_alex/',train_name{end-1},'/',train_name{end});
    imwrite(I2,filename);
end