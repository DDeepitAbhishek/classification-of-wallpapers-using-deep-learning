clc;
clear;

dataDir= './data/wallpapers/';
checkpointDir = 'modelCheckpoints/skinny_on_Original';

rng(1) % For reproducibility
Symmetry_Groups = {'P1', 'P2', 'PM' ,'PG', 'CM', 'PMM', 'PMG', 'PGG', 'CMM',...
    'P4', 'P4M', 'P4G', 'P3', 'P3M1', 'P31M', 'P6', 'P6M'};

% uncomment after you create the augmentation dataset
train_folder = 'train_original';
test_folder  = 'test_original';
fprintf('Loading Train Filenames and Label Data...'); t = tic;
train_all = imageDatastore(fullfile(dataDir,train_folder),'IncludeSubfolders',true,'LabelSource',...
    'foldernames');
train_all.Labels = reordercats(train_all.Labels,Symmetry_Groups);




%%
% Split with validation set
[train, val] = splitEachLabel(train_all,.9);
fprintf('Done in %.02f seconds\n', toc(t));

fprintf('Loading Test Filenames and Label Data...'); t = tic;
test = imageDatastore(fullfile(dataDir,test_folder),'IncludeSubfolders',true,'LabelSource',...
    'foldernames');
test.Labels = reordercats(test.Labels,Symmetry_Groups);
fprintf('Done in %.02f seconds\n', toc(t));

%%
rng('default');
numEpochs = 10; % 5 for both learning rates
batchSize = 250;
nTraining = length(train.Labels);

% Define the Network Structure, To add more layers, copy and paste the
% lines such as the example at the bottom of the code
%  CONV -> ReLU -> POOL -> FC -> DROPOUT -> FC -> SOFTMAX 
layers = [
    imageInputLayer([128 128 1]); % Input to the network is a 256x256x1 sized image 
    convolution2dLayer(5,40,'Padding',[2 2],'Stride', [1,1]);  % convolution layer with 40, 5x5 filters
    reluLayer();  % ReLU layer
    maxPooling2dLayer(2,'Stride',2); % Max pooling layer
    convolution2dLayer(5,40,'Padding',[1 1],'Stride', [1,1]);  % convolution layer with 40, 5x5 filters
    reluLayer();  % ReLU layer
    maxPooling2dLayer(2,'Stride',2); % Max pooling layer
    convolution2dLayer(3,80,'Padding',[1 1],'Stride', [1,1]);  % convolution layer with 80, 5x5 filters
    reluLayer();  % ReLU layer
    maxPooling2dLayer(2,'Stride',2); % Max pooling layer
    fullyConnectedLayer(25); % Fully connected with 25 layers
    dropoutLayer(0.25);
    fullyConnectedLayer(17); % Fully connected with 17 layers
    softmaxLayer(); % Softmax normalization layer
    classificationLayer(); % Classification layer
    ];

 if ~exist(checkpointDir,'dir'); mkdir(checkpointDir); end
% Set the training options
options = trainingOptions('sgdm','MaxEpochs',25,... 
    'InitialLearnRate',1e-3,...% learning rate
    'CheckpointPath', checkpointDir,...
    'MiniBatchSize', batchSize, ...
    'MaxEpochs',numEpochs);
    % uncommand and add the line below to the options above if you have 
    % version 17a or above to see the learning in realtime
    %'OutputFcn',@plotTrainingAccuracy,... 

% Train the network, info contains information about the training accuracy
% and loss
 t = tic;
[net1,info1] = trainNetwork(train,layers,options);
fprintf('Trained in in %.02f seconds\n', toc(t));

error_plot = figure;
plotTrainingAccuracy_All(info1,numEpochs);

% Test on the training data
train_labels = classify(net1,train);
train_acc = mean(train_labels==train.Labels);

train_con_mat = confusionmat(sort(grp2idx(train.Labels)), sort(grp2idx(train_labels)));
train_class_mat = train_con_mat./(meshgrid(countcats(train.Labels))');

% Test on the validation data
val_labels = classify(net1,val);
val_acc = mean(val_labels==val.Labels);

val_con_mat = confusionmat(sort(grp2idx(val.Labels)), sort(grp2idx(val_labels)));
val_class_mat = val_con_mat./(meshgrid(countcats(val.Labels))');

% Test on the Test data
test_labels = classify(net1,test);
test_acc = mean(test_labels==test.Labels);

test_con_mat = confusionmat(sort(grp2idx(test.Labels)), sort(grp2idx(test_labels)));
test_class_mat = test_con_mat./(meshgrid(countcats(test.Labels))');
